$lib =  -L/usr/X11R6/lib -L/usr/X11R6/lib64 -L/usr/local/lib -lX11

exefile : gltb.o glm.o
	gcc SphereMap.cpp -o SphereMap gltb.o glm.o $(lib) -lglut -lGL -lGLU -lm

glm.o:
	gcc -c glm.cpp -o glm.o $(lib) -lglut -lGL -lGLU -lm

gltb.o : 
	gcc -c gltb.cpp -o gltb.o $(lib) -lglut -lGL -lGLU -lm

clean:

	rm -f *.o SphereMap
